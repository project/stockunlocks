STOCKUNLOCKS - MOBILE UNLOCKING README

OVERVIEW:

Current Maintainer:

Darrell Henry (darrellhenry) - https://drupal.org/user/2264852

StockUnlocks is a module which has been designed to transform your Drupal site
into a remote mobile unlocking machine by connecting to any Dhru Fusion Client
via the remote API (Copyleft GPL, Dhru.com). For details regarding the Dhru
Client Remote API see http://wiki.dhru.com/Client_Remote_APi

The StockUnlocks module allows you to easily access the power and automation
that the Dhru API makes available to everyone.

If you're unlocking cell phones or other mobile devices without a management
utility, our goal is to free you from the spreadsheets and manual email
processing. The StockUnlocks module combined with Ubercart will allow you to
better focus your time and energy where they're needed the most.

Sign up for site access at https://www.stockunlocks.com to join the community
and to take advantage of our forums and issue tracking.


FEATURES:

* Access numerous unlocking services from multiple Providers.
* Import unlocking services directly into your own website.
* Automated processing of unlocking requests.
* Customize email responses.
* Short video overview: http://youtu.be/agB00UKz0g4

REQUIREMENTS:

* StockUnlocks depends on the Product module from Ubercart.

 - https://drupal.org/project/ubercart
 
* The PHP curl_url library is required as well. See:
http://php.net/manual/en/curl.setup.php

 - Many environments already have this installed by default.
 
* The "Private file system path" must be configured. The last two directory
entries must be named 'files/private'. e.g. 'sites/default/files/private'.
This directory must be writable by Drupal.

  - Your site settings are found here: admin/config/media/file-system
  - Working with files, Drupal 7: https://drupal.org/documentation/modules/file


INSTALLATION:

* Install as usual. For further information see:
http://drupal.org/documentation/install/modules-themes

  - Home page: https://www.stockunlocks.com/forums/stockunlocks-drupal-module
  - Documentation: https://www.stockunlocks.com/forums/documentation
  - Testing: https://www.stockunlocks.com/forums/installation-and-testing
  - Tutorials: https://www.youtube.com/stockunlocks


CONFIGURATION:

* To experience the full range of functionality, a free account should
be created at: http://reseller.stockunlocks.com/singup.html

  - Additional details will be provided upon account creation.
  - Your existing Service Provider, or others, may be used as well.

* Permission(s): 'Administer StockUnlocks' permission must be granted in order
to access the menus described below.

  - Access permissions: /admin/people/permissions
  - Locate the 'Administer StockUnlocks'
     permission under 'StockUnlocks mobile unlocking'

* Menu(s): The following menu items will be available after module installation.
  They will appear under "Navigation":

  - SU API Actions: /suapi/actions

    Work directly with a specific,
    designated Service Provider using the available API actions.

  - SU API Add: /suapi/add

    Create and enter the registration information specific
    to your Service Provider.

  - SU API Designated Provider: /suapi/data

    The Designated Provider allows you to directly interact with a particular
    Provider with the web browser using Actions.
    All Active Providers are still available for background website operations.

  - SU API Status: /suapi/status

    Displays and allows changing of the active/inactive status
    of registered Provider(s).

  - SU Email Templates: /suapi/emailtemplates

    Access and modify standard email messages sent to
    customers and administrators.


* View(s): The following view will be created after module installation:

  - StockUnlocks view

    This is a very basic view containing the published unlocking services.
    It requires the "Administer StockUnlocks" permission in order to access.


LIVE EXAMPLES:

* Unlock iPhone: https://www.stockunlocks.com/service/unlock-iphone-standard
* Unlock Blackberry: https://www.stockunlocks.com/all-blackberry
* Unlocking Services: https://www.stockunlocks.com/all-unlock-services

All the best,
The StockUnlocks Development Team

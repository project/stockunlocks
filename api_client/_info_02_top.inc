<?php

/**
 * @file
 * Common functionality for email templates.
 */

// These variables are not in global scope.
// This page is included within the context of existing functions,
// as a set of common variables and functionality used on various pages.
$su_emailtemplate_options = array();
$emailvals = array();

$su_emailtemplate_options = array(
  1 => 'Placed Order Success',
  2 => 'Placed Order Fail: Admin',
  3 => 'Checked Order Fail: Admin',
  4 => 'Order Available',
  5 => 'Order Rejected',
  6 => 'E-mail Mass Send',
);

$form['fieldset_first'] = array(
  '#type' => 'fieldset',
  '#weight' => -100,
  '#title' => t('API <strong>Email Templates</strong>:'),
  '#description' => t('Click <strong>"Edit"</strong> to view/modify the selected email template.'),
);

$suffix_api_emailtemplate = '<font color="red"><b>*</b></font>';
$form['fieldset_first']['api_emailtemplate'] = array(
  '#title' => t('API Email Templates:'),
  '#field_suffix' => $suffix_api_emailtemplate,
  '#type' => 'select',
  '#weight' => -100,
  '#empty_option' => t('Select...'),
  '#options' => $su_emailtemplate_options,
  '#default_value' => '',
  '#description' => t('Select to retrieve information.'),
);

$form['fieldset_first']['EDIT'] = array(
  '#type' => 'submit',
  '#weight' => -50,
  '#value' => t('Edit'),
);

<?php

/**
 * @file
 * Adds a new api Service Provider.
 */

include drupal_get_path('module', 'stockunlocks') . '/includes/stockunlocks.functions.api.inc';

/**
 * Form constructor for the add provider form.
 */
function stockunlocks_api_add_form($form, &$form_state) {

  $api_active = '';
  $api_site_name = '';
  $api_url = '';
  $api_username = '';
  $api_key = '';
  $api_notes = '';

  $form = array();

  $form['api_active'] = array(
    '#title' => t('Active:'),
    '#type' => 'select',
    '#required' => TRUE,
    '#weight' => -60,
    '#empty_option' => t('Select...'),
    '#options' => array(
      0 => t('No'),
      1 => t('Yes'),
    ),
    '#default_value' => $api_active,
    '#description' => t('Set this to <strong><em>Yes</em></strong> if you would like to activate this entry upon submission.'),
  );

  $form['api_site_name'] = array(
    '#title' => t("API Site Name:"),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#weight' => -50,
    '#default_value' => $api_site_name,
    '#description' => t('A short name description for this API Provider.'),
  );

  $form['api_url'] = array(
    '#title' => t("API URL:"),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#weight' => -40,
    '#default_value' => $api_url,
    '#description' => t('Format: <strong>http://www.url.com/</strong> or <strong>http://url.com/</strong>'),
  );

  $form['api_username'] = array(
    '#title' => t("API Username:"),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#weight' => -30,
    '#default_value' => $api_username,
    '#description' => t("The registered username used with the API provider's website above."),
  );

  $form['api_key'] = array(
    '#title' => t("API Access Key:"),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#weight' => -20,
    '#default_value' => $api_key,
    '#description' => t('The API Access Key assigned to you by this API provider.'),
  );

  $form['api_notes'] = array(
    '#title' => t("API Notes:"),
    '#type' => 'textarea',
    '#weight' => -10,
    '#default_value' => $api_notes,
    '#description' => t('Any notes about this API Provider.'),
  );

  $form['Add New'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );
  return $form;
}

/**
 * Implements hook_form_submit().
 */
function stockunlocks_api_add_form_submit($form, &$form_state) {
  switch ($form_state['triggering_element']['#value']) {
    case t('Add'):
      $api_info = array();
      $api_info['api_active'] = $form_state['values']['api_active'];
      $api_info['api_site_name'] = filter_xss($form_state['values']['api_site_name']);
      $api_info['api_url'] = filter_xss($form_state['values']['api_url']);
      $api_info['api_access_key'] = filter_xss($form_state['values']['api_key']);
      $api_info['api_username'] = filter_xss($form_state['values']['api_username']);
      $api_info['api_notes'] = filter_xss($form_state['values']['api_notes']);
      stockunlocks_add_table_provider($api_info);
      $path = 'suapi/status';
      $form_state['redirect'] = array($path);
      return;
  }
  $form_state['rebuild'] = TRUE;
}

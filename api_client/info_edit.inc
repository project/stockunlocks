<?php

/**
 * @file
 * Allows editing of a selected api Service Provider.
 */

include drupal_get_path('module', 'stockunlocks') . '/includes/stockunlocks.functions.api.inc';

/**
 * Form constructor for the api provider edit form.
 */
function stockunlocks_api_edit_form($form, &$form_state) {

  $api_provider = NULL;
  $api_active = '';
  $api_site_name = '';
  $api_url = '';
  $api_username = '';
  $api_key = '';
  $api_notes = '';
  $format_id = '';

  if (isset($_GET['apiid'])) {
    $api_provider = $_GET['apiid'];
  }

  if (!empty($api_provider) && $api_provider != NULL) {

    $results = db_query('SELECT ID, activeflag, sitename, url, apikey, username, notes FROM {su_api_provider} WHERE ID = :ID', array(':ID' => $api_provider));

    if ($results->rowCount() > 0) {
      foreach ($results as $row) {
        $api_provider = $row->ID;
        $api_active = $row->activeflag;
        $api_site_name = $row->sitename;
        $api_url = $row->url;
        $api_key = $row->apikey;
        $api_username = $row->username;
        $api_notes = $row->notes;
      }
    }
    else {
      $api_provider = NULL;
    }
  }

  $form = array();

  $form['api_provider'] = array(
    '#type' => 'hidden',
    '#value' => $api_provider,
  );

  $description = t('API Provider information.');
  if (empty($api_provider) && $api_provider == NULL) {
    $description = t('<strong>Provider API ID is missing. Now ADDING a NEW API Provider instead of EDITING!</strong>');
  }
  else {
    $description = t('<strong>"No" = Inactive</strong>: Decide whether to "Offline" or "Unpublish" the related services below.<br /><strong>"Yes" = Active</strong>: will enable the importing of services and supporting files.');
  }

  $form['api_first'] = array(
    '#type' => 'fieldset',
    '#weight' => -100,
    '#title' => t('API Provider <strong>status</strong>:'),
    '#description' => $description,
  );

  $form['api_first']['api_active'] = array(
    '#title' => t('Active:'),
    '#type' => 'select',
    '#required' => TRUE,
    '#weight' => -100,
    '#empty_option' => t('Select...'),
    '#options' => array(
      0 => t('No'),
      1 => t('Yes'),
    ),
    '#default_value' => $api_active,
    '#description' => t('Set this to <strong><em>Yes</em></strong> if you would like to activate this entry upon submission.'),
  );

  if (!empty($api_provider) && $api_provider != NULL) {

    $form['api_second'] = array(
      '#type' => 'fieldset',
      '#weight' => -90,
      '#title' => t('API Provider <strong>Services</strong>: (Only applies if <strong>"No"</strong> is selected above)'),
      '#description' => t('<strong>"Offline"</strong>: Services will still appear on the website, however cannot be purchased.<br /><strong>"Unpublish"</strong>
                          : Services will not appear on website.'),
    );

    $form['api_second']['deactivate-choice'] = array(
      '#type' => 'radios',
      '#weight' => -100,
      '#title' => t('What to do with the Services?'),
      '#description' => t('Specify your selection.'),
      '#options' => array(
        t('Offline'),
        t('Unpublish'),
      ),
      '#default_value' => 0,
    );
  }

  $form['api_site_name'] = array(
    '#title' => t("API Site Name:"),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#weight' => -50,
    '#default_value' => $api_site_name,
    '#description' => t('A short name description for this API Provider.'),
  );

  $form['api_url'] = array(
    '#title' => t("API URL:"),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#weight' => -40,
    '#default_value' => $api_url,
    '#description' => t('Format: <strong>http://www.url.com/</strong> or <strong>http://url.com/</strong>'),
  );

  $form['api_username'] = array(
    '#title' => t("API Username:"),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#weight' => -30,
    '#default_value' => $api_username,
    '#description' => t("The registered username used with the API provider's website above."),
  );

  $form['api_key'] = array(
    '#title' => t("API Access Key:"),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#weight' => -20,
    '#default_value' => $api_key,
    '#description' => t('The API Access Key assigned to you by this API provider.'),
  );

  $form['api_notes'] = array(
    '#title' => t("API Notes:"),
    '#type' => 'textarea',
    '#weight' => -10,
    '#default_value' => $api_notes,
    '#description' => t('Any notes about this API Provider.'),
  );

  if (!empty($api_provider) && $api_provider != NULL) {
    $form['first_fieldset']['Update'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
    );

  }
  else {

    $form['Add New'] = array(
      '#type' => 'submit',
      '#value' => t('Add'),
    );
  }
  return $form;
}

/**
 * Implements hook_form_submit().
 */
function stockunlocks_api_edit_form_submit($form, &$form_state) {
  switch ($form_state['triggering_element']['#value']) {
    case t('Update'):
      $api_deactivate_choice = $form_state['values']['deactivate-choice'];
      $form_api_info = array();
      $form_api_info['api_provider'] = $form_state['values']['api_provider'];
      $form_api_info['api_active'] = $form_state['values']['api_active'];
      $form_api_info['api_site_name'] = filter_xss($form_state['values']['api_site_name']);
      $form_api_info['api_url'] = filter_xss($form_state['values']['api_url']);
      $form_api_info['api_access_key'] = filter_xss($form_state['values']['api_key']);
      $form_api_info['api_username'] = filter_xss($form_state['values']['api_username']);
      $form_api_info['api_notes'] = filter_xss($form_state['values']['api_notes']);
      $the_one = stockunlocks_updateapiprovider($form_api_info, $api_deactivate_choice);
      $path = 'suapi/status';
      $form_state['redirect'] = array($path);
      drupal_set_message(t('<strong>@var</strong> API Profile updated.', array('@var' => $form_api_info['api_site_name'])));
      return;

    case t('Add'):
      $api_info = array();
      $api_info['api_active'] = $form_state['values']['api_active'];
      $api_info['api_site_name'] = filter_xss($form_state['values']['api_site_name']);
      $api_info['api_url'] = filter_xss($form_state['values']['api_url']);
      $api_info['api_access_key'] = filter_xss($form_state['values']['api_key']);
      $api_info['api_username'] = filter_xss($form_state['values']['api_username']);
      $api_info['api_notes'] = filter_xss($form_state['values']['api_notes']);
      stockunlocks_add_table_provider($api_info);
      $path = 'suapi/status';
      $form_state['redirect'] = array($path);
      return;
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Returns boolean, identifying whether or not designated provider.
 *
 * @param array $form_api_info
 *   An associative array containing:
 *   - api_provider: The int identifying the provider.
 *   - chosengetflag: The int designating the provider for actions.
 *   - api_url: The string representing the URL of the provider.
 *   - api_username: The string representing the username for URL access.
 *   - api_access_key: The string representing the access key for URL access.
 *   - api_notes: Notes particular to this entry.
 *
 * @param int $api_deactivate_choice
 *   0: Set the Service Provider's unlocking services to "Offline".
 *   1: "Unpublish" the Service Provider's unlocking services.
 */
function stockunlocks_updateapiprovider(&$form_api_info, $api_deactivate_choice) {
  $the_one = FALSE;
  $flag_update_api_full_class = FALSE;
  $flag_remove_api_full_class = FALSE;
  $results = db_query('SELECT ID, url, username, apikey FROM {su_api_provider} WHERE ID = :ID', array(':ID' => $form_api_info['api_provider']));

  if ($results->rowCount() > 0) {
    foreach ($results as $row) {
      $api_info = array();
      $api_info['api_provider'] = $row->ID;
      $api_info['api_url'] = $row->url;
      $api_info['api_username'] = $row->username;
      $api_info['api_access_key'] = $row->apikey;
      // Remove all of the Service Provider's local files.
      $results = stockunlocks_remove_functions_provider($api_info);
    }
  }

  $num_update = db_update('su_api_provider')
  ->fields(array(
    'activeflag' => $form_api_info['api_active'],
    'sitename' => $form_api_info['api_site_name'],
    'url' => $form_api_info['api_url'],
    'apikey' => $form_api_info['api_access_key'],
    'username' => $form_api_info['api_username'],
    'notes' => $form_api_info['api_notes'],
  ))
  ->condition('ID', $form_api_info['api_provider'])
  ->execute();

  switch ($form_api_info['api_active']) {
    case 0:
      $flag_remove_api_full_class = TRUE;
      switch ($api_deactivate_choice) {
        case 0:
          // "Offline" the Service Provider's services.
          stockunlocks_disable_provider_services_offline(stockunlocks_get_provider_services_nids($form_api_info));
          break;

        case 1:
          // "Unpublish" the Service Provider's services.
          stockunlocks_disable_provider_services_unpublish(stockunlocks_get_provider_services_nids($form_api_info));
          break;

      }
      break;

    case 1:
      // ACTIVATE = Create the dynamic files.
      $results = stockunlocks_add_functions_provider($form_api_info);
      break;

  }

  $chosengetflag_id = db_query('SELECT ID FROM {su_api_provider} WHERE chosengetflag = :chosengetflag', array(':chosengetflag' => 1))->fetchField();

  // If this is the designated Service Provider,
  // modify the api_full_dhru.inc file.
  if ($form_api_info['api_provider'] == $chosengetflag_id) {
    $the_one = TRUE;
    if (($api_info['api_url'] != $form_api_info['api_url']) || ($api_info['api_username'] != $form_api_info['api_username']) || ($api_info['api_access_key'] != $form_api_info['api_access_key'])) {
      $flag_update_api_full_class = TRUE;
    }
  }

  if ($the_one) {

    if ($flag_remove_api_full_class) {
      stockunlocks_remove_local_files_provider();
    }
    else {
      if ($flag_update_api_full_class) {
        // Modify the default api_full_dhru.inc file.
        $results = stockunlocks_add_local_files_provider($form_api_info);
      }
    }
  }
  return $the_one;
}

<?php

/**
 * @file
 * Displays/enables the active/inactive status of Provider(s)
 */

include drupal_get_path('module', 'stockunlocks') . '/includes/stockunlocks.functions.api.inc';

/**
 * Form constructor for the api status form.
 */
function stockunlocks_api_status_form($form, &$form_state) {
  $form = array();
  $form['#after_build'][] = 'stockunlocks_api_status_after_build';

  $providers = array();
  $i = 0;
  $result = db_query('SELECT * FROM {su_api_provider} n');

  foreach ($result as $item) {
    if ($item->activeflag == 1) {
      $providers[$i]['api_status'] = 'Active';
    }
    else {
      $providers[$i]['api_status'] = 'Inactive';
    }
    $providers[$i]['uid']  = $item->ID;
    $providers[$i]['site_name']  = '<a href="/suapi/edit?apiid=' . $item->ID . '">' . $item->sitename . '</a>';
    $providers[$i]['url_name']  = $item->url;
    $providers[$i]['api_key']  = $item->apikey;
    $providers[$i]['user_name']  = $item->username;
    $providers[$i]['notes']  = $item->notes;
    ++$i;
  }

  $header = array(
    'api_status' => t('Status'),
    'site_name' => t('Name'),
    'url_name' => t('URL'),
    'api_key' => t('API Key'),
    'user_name' => t('Username'),
    'notes' => t('Notes'),
  );

  $options = array();
  foreach ($providers as $provider) {
    $options[$provider['uid']] = array(
      'api_status' => $provider['api_status'],
      'site_name' => $provider['site_name'],
      'url_name' => $provider['url_name'],
      'api_key' => $provider['api_key'],
      'user_name' => $provider['user_name'],
      'notes' => $provider['notes'],
    );
  }

  $form['table'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No API Providers found. <a href="/suapi/add">Click here to ADD an API Provider</a>.'),
    '#markup' => theme('pager'),
  );

  if (!empty($options) && $options != NULL) {

    $form['fieldset_delete_confirm'] = array(
      '#type' => 'fieldset',
      '#weight' => -200,
      '#title' => t('<strong>Deleting API Profile(s) ...</strong>'),
      '#description' => t('Click <strong>"Delete Profile(s)"</strong> to permanently remove ALL services and related records.<br /><br />
                          This action cannot be undone.'),
    );

    $form['fieldset_delete_confirm']['confirm_delete'] = array(
      '#type' => 'submit',
      '#weight' => -100,
      '#value' => t('Delete Profile(s)'),
    );

    $form['fieldset_delete_confirm']['cancel_delete'] = array(
      '#type' => 'submit',
      '#weight' => -90,
      '#value' => t('Cancel'),
    );

    $form['api_first'] = array(
      '#type' => 'fieldset',
      '#title' => t('Activate or Delete the <strong>selected</strong> API Provider(s):'),
      '#description' => t('<strong>Activate</strong>: will enable the importing of services and related records.<br />
                          <strong>Delete</strong>: will permanently remove ALL services and related records.'),
    );

    $form['api_first']['activate'] = array(
      '#type' => 'submit',
      '#weight' => -100,
      '#value' => t('Activate'),
    );

    $form['api_first']['delete'] = array(
      '#type' => 'submit',
      '#weight' => -90,
      '#value' => t('Delete'),
    );

    $form['api_second'] = array(
      '#type' => 'fieldset',
      '#title' => t('Deactivate the <strong>selected</strong> API Provider(s):'),
      '#description' => t('<strong>"Offline"</strong>: Services will still appear on the website, however cannot be purchased.<br />
      <strong>"Unpublish"</strong>: Services will not appear on website, however will still be available for editing.'),
    );

    $form['api_second']['deactivate-choice'] = array(
      '#type' => 'radios',
      '#weight' => -100,
      '#title' => t('What to do with the Services?'),
      '#description' => t('Specify your selection.'),
      '#options' => array(
        t('Offline'),
        t('Unpublish'),
      ),
      '#default_value' => 0,
    );

    $form['api_second']['deactivate'] = array(
      '#type' => 'submit',
      '#weight' => -90,
      '#value' => t('Deactivate'),
    );
  }
  return $form;
}

/**
 * Custom after build function.
 */
function stockunlocks_api_status_after_build($form, &$form_state) {
  $form['fieldset_delete_confirm']['#access'] = FALSE;
  if (!empty($form_state['triggering_element']['#value'])) {
    switch ($form_state['triggering_element']['#value']) {
      case t('Delete'):
        $table_api_info = array();
        $results = array_filter($form_state['values']['table']);
        $i = 0;
        foreach ($results as $key => $value) {
          $table_api_info[$i] = $value;
          ++$i;
        }
        if (empty($table_api_info) && $table_api_info == NULL) {
          form_set_error('delete', t('Delete: Nothing Selected!'));
          break;

        }
        $form['fieldset_delete_confirm']['#access'] = TRUE;
        break;

    }
  }
  return $form;
}

/**
 * Implements hook_form_submit().
 */
function stockunlocks_api_status_form_submit($form, &$form_state) {

  switch ($form_state['triggering_element']['#value']) {
    case t('Activate'):
      $table_api_info = array();
      $results = array_filter($form_state['values']['table']);
      $i = 0;
      foreach ($results as $key => $value) {
        $table_api_info[$i] = $value;
        ++$i;
      }
      if (!empty($table_api_info) && $table_api_info != NULL) {
        stockunlocks_tableactivateapiprovider($table_api_info);
      }
      else {
        form_set_error('activate', t('Activate: Nothing Selected!'));
      }
      return;

    case t('Deactivate'):
      $table_api_info = array();
      $api_deactivate_choice = $form_state['values']['deactivate-choice'];
      $results = array_filter($form_state['values']['table']);
      $i = 0;
      foreach ($results as $key => $value) {
        $table_api_info[$i] = $value;
        ++$i;
      }
      if (!empty($table_api_info) && $table_api_info != NULL) {
        stockunlocks_table_deactivate_api_provider($table_api_info, $api_deactivate_choice);
      }
      else {
        form_set_error('deactivate', t('Deactivate: Nothing Selected!'));
      }
      return;

    case t('Delete Profile(s)'):
      $table_api_info = array();
      $results = array_filter($form_state['values']['table']);
      $i = 0;
      foreach ($results as $key => $value) {
        $table_api_info[$i] = $value;
        ++$i;
      }
      if (!empty($table_api_info) && $table_api_info != NULL) {
        stockunlocks_tabledeleteapiprovider($table_api_info);
      }
      else {
        form_set_error('delete', t('Delete: Nothing Selected!'));
      }
      return;

    case t('Cancel'):
      drupal_set_message(t('Action cancelled.'));
      return;

  }
  form_set_error('Delete', t('ARE YOU SURE YOU WANT TO DELETE THE SELECTED ITEM(S)?'));
  $form_state['rebuild'] = TRUE;
}

/**
 * Activate a specific Service Provider for all browser based activity.
 *
 * @param array $table_api_info
 *   An indexed array containing:
 *   - ID: The int identifying the service provider.
 */
function stockunlocks_tableactivateapiprovider(&$table_api_info) {
  $i = 0;
  $sitename = '';
  $format_id = '';

  foreach ($table_api_info as $key => $value) {

    $activeflag = db_query('SELECT activeflag FROM {su_api_provider} WHERE ID = :ID', array(':ID' => $value))->fetchField();

    // If this is already the designated Service Provider, leave it alone.
    if (!$activeflag == 1) {
      $num_updated = NULL;
      // Activate the entry.
      $num_updated = db_update('su_api_provider')
      ->fields(array(
      'activeflag' => 1,
      ))
      ->condition('ID', $value)
      ->execute();

      $results = db_query('SELECT ID, sitename, url, username, apikey FROM {su_api_provider} WHERE ID = :ID', array(':ID' => $value));

      if ($results->rowCount() > 0) {
        foreach ($results as $row) {
          $api_info = array();
          $api_info['api_provider'] = $row->ID;
          $api_info['sitename'] = $row->sitename;
          $api_info['api_url'] = $row->url;
          $api_info['api_username'] = $row->username;
          $api_info['api_access_key'] = $row->apikey;
          $sitename = $sitename . $row->sitename . ',';
          // Create all local API files.
          $results = stockunlocks_add_functions_provider($api_info);
        }
      }
      ++$i;
    }
  }
  $msg = t('Activation done for @var1 Profile(s):<br />@var2',
                    array(
                      '@var1' => $i,
                      '@var2' => $sitename,
                    ));
  drupal_set_message($msg);
}

/**
 * Delete a specific Service Provider and all related local files.
 *
 * @param array $table_api_info
 *   An indexed array containing:
 *   - ID: The int identifying the service provider.
 */
function stockunlocks_tabledeleteapiprovider(&$table_api_info) {
  $i = 0;
  $sitename = '';
  $format_id = '';
  $the_one = FALSE;
  $form_api_info = array();
  $apiids = array();
  foreach ($table_api_info as $key => $value) {
    $form_api_info['api_provider'] = $value;
    $apiids[] = $value;

    $chosengetflag_id = db_query('SELECT ID FROM {su_api_provider} WHERE chosengetflag = :chosengetflag', array(':chosengetflag' => 1))->fetchField();

    // If this is the designated Service Provider,
    // modify the api_full_dhru.inc file.
    if ($value == $chosengetflag_id) {
      $the_one = TRUE;
      stockunlocks_remove_local_files_provider();
    }

    $results = db_query('SELECT ID, sitename, url, username, apikey FROM {su_api_provider} WHERE ID = :ID', array(':ID' => $value));

    if ($results->rowCount() > 0) {
      foreach ($results as $row) {
        $api_info = array();
        $api_info['api_provider'] = $row->ID;
        $api_info['api_url'] = $row->url;
        $api_info['api_username'] = $row->username;
        $api_info['api_access_key'] = $row->apikey;
        $sitename = $sitename . $row->sitename . ',';
        // Remove all of the Service Provider's local files.
        $results_remove = stockunlocks_remove_functions_provider($api_info);
      }
    }

    // Delete all of the related table entries.
    $num_deleted = NULL;
    $num_deleted = db_delete('su_api_provider')
    ->condition('ID', $value)
    ->execute();
    // Delete the related unlocking services (products).
    stockunlocks_delete_provider_services(stockunlocks_get_provider_services_nids($form_api_info));
    // Delete the related brands/models.
    stockunlocks_delete_provider_models($apiids);
    // Delete the related MEP entries.
    stockunlocks_delete_provider_mep($apiids);
    // Delete the related country and network providers.
    stockunlocks_delete_provider_networks($apiids);
    ++$i;
  }
  $msg = t('Deleted @var1 Profile(s) and related entries:<br />@var2',
                    array(
                      '@var1' => $i,
                      '@var2' => $sitename,
                    ));
  drupal_set_message($msg);
}

/**
 * Deactivate a specific Service Provider, removing all related local files.
 *
 * @param array $table_api_info
 *   An indexed array containing:
 *   - ID: The int identifying the service provider.
 *
 * @param int $api_deactivate_choice
 *   0: Set the Service Provider's unlocking services to "Offline".
 *   1: "Unpublish" the Service Provider's unlocking services.
 */
function stockunlocks_table_deactivate_api_provider(&$table_api_info, $api_deactivate_choice) {
  $i = 0;
  $sitename = '';
  $format_id = '';
  $the_one = FALSE;
  $form_api_info = array();
  foreach ($table_api_info as $key => $value) {
    $form_api_info['api_provider'] = $value;

    $activeflag = db_query('SELECT activeflag FROM {su_api_provider} WHERE ID = :ID', array(':ID' => $value))->fetchField();
    $chosengetflag_id = db_query('SELECT ID FROM {su_api_provider} WHERE chosengetflag = :chosengetflag', array(':chosengetflag' => 1))->fetchField();

    if ($value == $chosengetflag_id) {
      $the_one = TRUE;
    }
    // Only modify active entries.
    if ($activeflag == 1) {
      $num_updated = NULL;

      // De-activate the entry.
      $num_updated = db_update('su_api_provider')
      ->fields(array(
      'activeflag' => 0,
      ))
      ->condition('ID', $value)
      ->execute();

      // Remove the designated Service Provider status.
      $num_updated = db_update('su_api_provider')
      ->fields(array(
      'chosengetflag' => 0,
      ))
      ->condition('ID', $value)
      ->execute();

      $results = db_query('SELECT ID, sitename, url, username, apikey FROM {su_api_provider} WHERE ID = :ID', array(':ID' => $value));

      if ($results->rowCount() > 0) {
        foreach ($results as $row) {
          $api_info = array();
          $api_info['api_provider'] = $row->ID;
          $api_info['api_url'] = $row->url;
          $api_info['api_username'] = $row->username;
          $api_info['api_access_key'] = $row->apikey;
          $sitename = $sitename . $row->sitename . ',';
          // Remove all of the Service Provider's local files.
          $results = stockunlocks_remove_functions_provider($api_info);
        }

        if ($the_one) {
          stockunlocks_remove_local_files_provider();
        }
      }
      ++$i;
    }

    switch ($api_deactivate_choice) {
      case 0:
        // "Offline" the Service Provider's services.
        stockunlocks_disable_provider_services_offline(stockunlocks_get_provider_services_nids($form_api_info));
        break;

      case 1:
        // "Unpublish" the Service Provider's services.
        stockunlocks_disable_provider_services_unpublish(stockunlocks_get_provider_services_nids($form_api_info));
        break;

    }
  }
  $msg = t('Deactivation done for @var1 Profile(s):<br />@var2',
                    array(
                      '@var1' => $i,
                      '@var2' => $sitename,
                    ));
  drupal_set_message($msg);
}

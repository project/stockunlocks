<?php

/**
 * @file
 * Displays the results of a submitted order for the Provider's service.
 */

/**
 * Form constructor for the api order results form.
 */
function stockunlocks_api_orderresults_form($form, &$form_state) {
  $form = array();

  $format_id = '';

  if (!empty($_SESSION['$stockunlocks_$session_order_results'])) {
    switch ($_SESSION['$stockunlocks_$session_order_results']) {
      case 'ERROR':
        $order_message = $_SESSION['$stockunlocks_$session_order_message'];
        $order_description = $_SESSION['$stockunlocks_$session_order_description'];
        $order_apiid = $_SESSION['$stockunlocks_$session_order_apiid'];
        $order_imei = $_SESSION['$stockunlocks_$session_order_imei'];
        $title = t('<strong>ORDER RESULTS : ERROR - @var</strong>', array('@var' => $order_message));
        $description = t('API ID# : @var1, IMEI : @var2<br /><br />Error Description : @var3<br />Click <a href = "/suapi/actions"><strong>HERE</strong></a> to continue.',
                          array(
                            '@var1' => $order_apiid,
                            '@var2' => $order_imei,
                            '@var3' => $order_description,
                          ));
        $form['fieldset_orderresults_error'] = array(
          '#type' => 'fieldset',
          '#weight' => -200,
          '#title' => $title,
          '#description' => $description,
        );
        break;

      case 'SUCCESS':
        $order_message = $_SESSION['$stockunlocks_$session_order_message'];
        $order_referenceid = $_SESSION['$stockunlocks_$session_order_referenceid'];
        $order_apiid = $_SESSION['$stockunlocks_$session_order_apiid'];
        $order_imei = $_SESSION['$stockunlocks_$session_order_imei'];
        $title = t('<strong>ORDER MESSAGE : @var</strong>', array('@var' => $order_message));
        $description = t('<strong>Order# : @var1</strong>, API ID# : @var2, IMEI : @var3<br /><br />Click <a href = "/suapi/actions"><strong>HERE</strong></a> to continue.',
                          array(
                            '@var1' => $order_referenceid,
                            '@var2' => $order_apiid,
                            '@var3' => $order_imei,
                          ));
        $form['fieldset_orderresults_success'] = array(
          '#type' => 'fieldset',
          '#weight' => -200,
          '#title' => $title,
          '#description' => $description,
        );
        break;

    }
  }
  else {
    $form['fieldset_orderresults_none'] = array(
      '#type' => 'fieldset',
      '#weight' => -200,
      '#title' => t('<strong>NO ORDER RESULTS AVAILABLE</strong>'),
      '#description' => t('Click <a href = "/suapi/actions"><strong>HERE</strong></a> to continue.'),
    );
  }
  return $form;
}

/**
 * Implements hook_form_validate().
 */
function stockunlocks_api_orderresults_form_validate($form, &$form_state) {

}

/**
 * Implements hook_form_submit().
 */
function stockunlocks_api_orderresults_form_submit($form, &$form_state) {

  $form_state['rebuild'] = TRUE;
}

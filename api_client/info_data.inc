<?php

/**
 * @file
 * Allows selection of a designated api Provider
 */

include drupal_get_path('module', 'stockunlocks') . '/includes/stockunlocks.functions.api.inc';

/**
 * Form constructor for the api service provider form.
 */
function stockunlocks_api_data_form($form, &$form_state) {
  $form = array();
  $default_value = db_query('SELECT ID FROM {su_api_provider} WHERE chosengetflag = :chosengetflag', array(':chosengetflag' => 1))->fetchField();
  $result = db_query('SELECT ID, activeflag, sitename, url, apikey, username, notes FROM {su_api_provider} WHERE activeflag = :activeflag', array(':activeflag' => 1));
  $providers = array();
  $i = 0;

  foreach ($result as $item) {
    if ($item->activeflag == 1) {
      $providers[$i]['api_status'] = 'Active';
    }
    else {
      $providers[$i]['api_status'] = 'Inactive';
    }
    $providers[$i]['uid']  = $item->ID;
    $providers[$i]['site_name']  = '<a href="/suapi/edit?apiid=' . $item->ID . '">' . $item->sitename . '</a>';
    $providers[$i]['url_name']  = $item->url;
    $providers[$i]['api_key']  = $item->apikey;
    $providers[$i]['user_name']  = $item->username;
    $providers[$i]['notes']  = $item->notes;
    ++$i;
  }

  $header = array(
    'api_status' => t('Status'),
    'site_name' => t('Name'),
    'url_name' => t('URL'),
    'api_key' => t('API Key'),
    'user_name' => t('Username'),
    'notes' => t('Notes'),
  );

  $options = array();
  foreach ($providers as $provider) {
    $options[$provider['uid']] = array(
      'api_status' => $provider['api_status'],
      'site_name' => $provider['site_name'],
      'url_name' => $provider['url_name'],
      'api_key' => $provider['api_key'],
      'user_name' => $provider['user_name'],
      'notes' => $provider['notes'],
    );
  }
  $form['table'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#default_value' => $default_value,
    '#multiple' => FALSE,
    '#js_select' => FALSE,
    '#empty' => t('No active API Providers found. Click to either:  <a href="/suapi/add">ADD an API Provider</a> OR <a href="/suapi/status">MODIFY an API Provider</a>.'),
  );

  if ($result->rowCount() > 0) {
    $form['Select'] = array(
      '#type' => 'submit',
      '#value' => t('Select'),
    );
  }

  return $form;
}

/**
 * Implements hook_form_submit().
 */
function stockunlocks_api_data_form_submit($form, &$form_state) {
  switch ($form_state['triggering_element']['#value']) {
    case t('Select'):
      $table_api_info = array();
      $results = $form_state['values']['table'];
      $table_api_info[] = $results;
      if (!empty($table_api_info) && $table_api_info != NULL) {
        stockunlocks_table_set_get_api_provider($table_api_info);
      }
      return;
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Designate a specific Service Provider for all browser based activity.
 *
 * @param array $table_api_info
 *   An indexed array containing:
 *   - ID: The int identifying the service provider.
 */
function stockunlocks_table_set_get_api_provider(&$table_api_info) {
  $i = 0;
  $sitename = '';
  foreach ($table_api_info as $key => $value) {

    $results = db_query('SELECT ID, sitename, url, username, apikey FROM {su_api_provider} WHERE ID = :ID', array(':ID' => $value));

    if ($results->rowCount() > 0) {
      foreach ($results as $row) {
        $api_info = array();
        $api_info['api_provider'] = $row->ID;
        $api_info['api_url'] = $row->url;
        $api_info['api_username'] = $row->username;
        $api_info['api_access_key'] = $row->apikey;
        $sitename = $row->sitename;
        // Modify the default api_full_dhru.inc.
        $results = stockunlocks_add_local_files_provider($api_info);
      }
    }

    $chosengetflag_id = db_query('SELECT ID FROM {su_api_provider} WHERE chosengetflag = :chosengetflag', array(':chosengetflag' => 1))->fetchField();

    // If this is already the designated Service Provider, leave it alone.
    if ($value != $chosengetflag_id) {

      if (!empty($chosengetflag_id) && $chosengetflag_id != NULL) {
        // Reset the previous designated Service Provider.
        $num_update = db_update('su_api_provider')
        ->fields(array(
        'chosengetflag' => 0,
        ))
        ->condition('ID', $chosengetflag_id)
        ->execute();
      }

      // Designate the new Service Provider.
      $num_update = db_update('su_api_provider')
      ->fields(array(
      'chosengetflag' => 1,
      ))
      ->condition('ID', $value)
      ->execute();
    }
  }
  drupal_set_message(t('<strong>@var</strong> designated as the API Provider for Actions', array('@var' => $sitename)));
}

<?php

/**
 * @file
 * Common functionality for api actions.
 */

// Resets the session values representing order results, if available.
// These session values are used in the info_orderresults.inc file.
// Used for presenting the results of an order submitted via the web browser.
if (!empty($_SESSION['$stockunlocks_$session_order_results'])) {
  unset($_SESSION['$stockunlocks_$session_order_results']);
}
// Retrieve the name of the designated Service Provider.
$the_one = db_query('SELECT sitename FROM {su_api_provider} WHERE chosengetflag = :chosengetflag', array(':chosengetflag' => 1))->fetchField();

// These variables are not in global scope.
// This page is included within the context of existing functions,
// as a set of common variables and functionality used on various pages.
$api_action_apiid = '';
$api_action_imei = '';
$api_action_orderid = '';
$show_contents = TRUE;
$active_api = FALSE;
$request = array();
$services = array();

$api = new StockunlocksDhruFusion();

$api->debug = TRUE;

$active_arr = array('MSG' => 'ERROR', 'MESSAGE' => 'Authentication Failed');

$api_action_options = array();

$api_action_options = array(
  1 => 'Account Info',
  2 => 'IMEI Service List',
  3 => 'Get IMEI Order',
  4 => 'MEP List',
  5 => 'Model List',
  6 => 'IMEI Service Details',
  7 => 'Network Provider List',
  11 => 'Place IMEI Order',
);
$request = $api->action('accountinfo');
$i = 0;
// Confirm that there is a valid API connection.
if (is_array($request)) {
  $iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($request), RecursiveIteratorIterator::SELF_FIRST);
  foreach ($iterator as $key1 => $val1) {
    if ($key1 === 'ERROR') {
      $active_arr = array('MSG' => 'ERROR', 'MESSAGE' => 'Authentication Failed');
    }
    if ($key1 === 'SUCCESS') {
      if (is_array($val1)) {
        foreach ($val1 as $key2 => $val2) {
          if (is_array($val2)) {
            foreach ($val2 as $key3 => $val3) {
              if ($key3 === 'message') {
                $tmp_msg = $val3;
              }
              if ($key3 === 'AccoutInfo') {
                $active_api = TRUE;
                if (is_array($val3)) {
                  $tmp_credit = '';
                  $tmp_mail = '';
                  $tmp_currency = '';
                  foreach ($val3 as $key4 => $val4) {
                    if ($key4 === 'credit') {
                      $tmp_credit = $val4;
                    }
                    if ($key4 === 'mail') {
                      $tmp_mail = $val4;
                    }
                    if ($key4 === 'currency') {
                      $tmp_currency = $val4;
                    }
                    if ($tmp_currency != '') {
                      $active_arr = array(
                        'MSG' => $tmp_msg,
                        'CREDIT' => $tmp_credit,
                        'MAIL' => $tmp_mail,
                        'CURRENCY' => $tmp_currency,
                      );
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

if (!$active_api) {
  $show_contents = FALSE;
  $form['invalidiprequest'] = array(
    '#type' => 'fieldset',
    '#title' => $active_arr['MSG'],
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $header = array(
    'message' => t('Message: @var', array('@var' => $active_arr['MESSAGE'])),
  );
  $options = array();
  $options[0] = array(
    'message' => t('No designated/active API Providers found or a failed connection. Click to either:  <a href="/suapi/add">ADD</a>, <a href="/suapi/data">DESIGNATE</a> OR <a href="/suapi/status">MODIFY an API Provider</a>.'),
  );
  $form['invalidiprequest']['table'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#multiple' => FALSE,
    '#js_select' => FALSE,
    '#disabled' => TRUE,
  );
}
else {
  $title = t('<strong>@var</strong> is the Designated API Provider for Actions.', array('@var' => $the_one));
  $form['fieldset_provider'] = array(
    '#type' => 'fieldset',
    '#weight' => -200,
    '#title' => $title,
    '#description' => t('Click <a href = "/suapi/data"><strong>HERE</strong></a> to change.'),
  );

  $form['fieldset_first'] = array(
    '#type' => 'fieldset',
    '#weight' => -100,
    '#title' => t('API <strong>Actions</strong>:'),
    '#description' => t('Click <strong>"Execute"</strong> to carry out the selected action.'),
  );

  $form['fieldset_first']['api_action'] = array(
    '#title' => t('API Actions:'),
    '#type' => 'select',
    '#weight' => -100,
    '#empty_option' => t('Select...'),
    '#options' => $api_action_options,
    '#default_value' => '',
    '#description' => t('Select to retrieve or submit information.'),
  );

  $form['fieldset_first']['apiid'] = array(
    '#title' => t("Service API ID:"),
    '#type' => 'textfield',
    '#weight' => -80,
    '#default_value' => '',
  );

  $form['fieldset_first']['imei'] = array(
    '#title' => t("IMEI to be submitted:"),
    '#type' => 'textfield',
    '#weight' => -70,
    '#default_value' => '',
  );

  $form['fieldset_first']['orderid'] = array(
    '#title' => t("Order ID to check:"),
    '#type' => 'textfield',
    '#weight' => -60,
    '#default_value' => '',
  );

  $form['fieldset_first']['API DO'] = array(
    '#type' => 'submit',
    '#weight' => -50,
    '#value' => t('Execute'),
  );

}

<?php

/**
 * @file
 * Directs actions for the designated Provider.
 */

/**
 * Form constructor for the actions form.
 */
function stockunlocks_api_actions_form($form, &$form_state) {
  $form = array();

  include '_info_01_top.inc';
  return $form;
}

/**
 * Implements hook_form_validate().
 */
function stockunlocks_api_actions_form_validate($form, &$form_state) {
  $flag_continue_apiid = TRUE;
  $flag_continue_imei = TRUE;
  $flag_continue_orderid = TRUE;
  $flag_msg_apiid = '';
  $flag_msg_imei = '';
  $flag_msg_orderid = '';

  if ($form_state['triggering_element']['#value'] == 'Execute') {
    $order_id_val = filter_xss($form_state['values']['orderid']);
    $apiid_val = filter_xss($form_state['values']['apiid']);
    $imei_val = filter_xss($form_state['values']['imei']);
    if (!empty($form_state['values']['api_action'])) {
      $api_action_val = $form_state['values']['api_action'];
      // Order ID is Required. User selected "Get IMEI Order".
      if (($api_action_val == '3') && $order_id_val == '') {
        $flag_continue_orderid = FALSE;
        $flag_msg_orderid = t('Order ID is required.<br />');
      }
      else {
        if (($api_action_val == '3') && (stockunlocks_is_digits($order_id_val) != 1)) {
          $flag_continue_orderid = FALSE;
          $flag_msg_orderid = t('Order ID should be digits only: no letters, punctuation, or spaces.<br />');
        }
      }
      // API ID is Required: User selected retrieve.
      // "Model list" or "Provider list".
      if ((($api_action_val == '5') || ($api_action_val == '7')) && $apiid_val != '') {
        if (stockunlocks_is_digits($apiid_val) != 1) {
          $flag_continue_apiid = FALSE;
          $flag_msg_apiid = t('API ID should be digits only: no letters, punctuation, or spaces.<br />');
        }
      }
      // API ID is Required. User requested "IMEI Service Details".
      if (($api_action_val == '6') && $apiid_val == '') {
        $flag_continue_apiid = FALSE;
        $flag_msg_apiid = t('Service API ID is required.<br />');
      }
      else {
        if (($api_action_val == '6') && (stockunlocks_is_digits($apiid_val) != 1)) {
          $flag_continue_apiid = FALSE;
          $flag_msg_apiid = t('API ID should be digits only: no letters, punctuation, or spaces.<br />');
        }
      }
      // IMEI & API ID are Required. User submitted "Place IMEI Order".
      if (($api_action_val == '11') && ($imei_val == '' || $apiid_val == '')) {
        if ($imei_val == '') {
          $flag_continue_imei = FALSE;
          $flag_msg_imei = t('IMEI is required<br />');
        }
        if ($apiid_val == '') {
          $flag_continue_apiid = FALSE;
          $flag_msg_apiid = t('Service API ID is required<br />');
        }
      }
      else {
        if (($api_action_val == '11') && (stockunlocks_is_digits($apiid_val) != 1)) {
          $flag_continue_apiid = FALSE;
          $flag_msg_apiid = t('API ID should be digits only: no letters, punctuation, or spaces.<br />');
        }
        if (($api_action_val == '11') && (stockunlocks_is_digits($imei_val) != 1)) {
          $flag_continue_imei = FALSE;
          $flag_msg_imei = t('IMEI should be digits only: no letters, punctuation, or spaces.<br />');
        }
        if (($api_action_val == '11') && (strlen($imei_val) == 15)) {
          if (stockunlocks_check_imei($imei_val) == 0) {
            $flag_continue_imei = FALSE;
            $flag_msg_imei = t('@var is not a valid IMEI entry.<br />', array('@var' => $imei_val));
          }
        }
      }

      if (!$flag_continue_orderid) {
        form_set_error('orderid', $flag_msg_orderid);
      }
      if (!$flag_continue_apiid) {
        form_set_error('apiid', $flag_msg_apiid);
      }
      if (!$flag_continue_imei) {
        form_set_error('imei', $flag_msg_imei);
      }
    }
    else {
      form_set_error('api_action', t('API Actions is required.'));
    }
  }
}

/**
 * Implements hook_form_submit().
 */
function stockunlocks_api_actions_form_submit($form, &$form_state) {
  switch ($form_state['triggering_element']['#value']) {
    case t('Execute'):
      $selected = $form_state['values']['api_action'];
      if ($selected == 0) {
      }
      else {
        $path = 'suapi/actions';
        $serviceid = filter_xss(trim($form_state['values']['apiid']));
        $imei = filter_xss(trim($form_state['values']['imei']));
        $orderid = filter_xss(trim($form_state['values']['orderid']));
        switch ($selected) {
          case '1':
            drupal_set_message(t('Account information retrieved.'));
            $path = 'suapi/accountinfo';
            $form_state['redirect'] = array($path);
            return;

          case '2':
            drupal_set_message(t('IMEI Service List retrieved.'));
            $path = 'suapi/imeiservicelist';
            $form_state['redirect'] = array($path);
            return;

          case '3':
            $path = 'suapi/getimeiorder';
            $query = array('orderid' => $orderid);
            $form_state['redirect'] = array($path, array('query' => $query));
            drupal_set_message(t('IMEI Order retrieved: Order#  @var', array('@var' => $orderid)));
            return;

          case '4':
            drupal_set_message(t('MEP List retrieved.'));
            $path = 'suapi/meplist';
            $form_state['redirect'] = array($path);
            return;

          case '5':
            $d_message = '';
            $path = 'suapi/modellist';
            $query = array('serviceid' => $serviceid);
            $form_state['redirect'] = array($path, array('query' => $query));
            if (empty($serviceid) && $serviceid == NULL) {
              $d_message = t('Model List retrieved. No Service ID# supplied.');
            }
            else {
              $d_message = t('Model List retrieved: Service ID# @var', array('@var' => $serviceid));
            }
            drupal_set_message($d_message);
            return;

          case '6':
            $path = 'suapi/getimeiservicedetails';
            $query = array('serviceid' => $serviceid);
            $form_state['redirect'] = array($path, array('query' => $query));
            drupal_set_message(t('IMEI Service Details retrieved: Service ID# @var', array('@var' => $serviceid)));
            return;

          case '7':
            $d_message = '';
            $path = 'suapi/providerlist';
            $query = array('serviceid' => $serviceid);
            $form_state['redirect'] = array($path, array('query' => $query));
            if (empty($serviceid) && $serviceid == NULL) {
              $d_message = t('Provider List retrieved. No Service ID# supplied.');
            }
            else {
              $d_message = t('Provider List retrieved: Service ID# @var', array('@var' => $serviceid));
            }
            drupal_set_message($d_message);
            return;

          case '8':
            drupal_set_message(t('File Order, dropdown selection: @var', array('@var' => $selected)));
            $path = 'suapi/getfileorder';
            $form_state['redirect'] = array($path);
            return;

          case '9':
            drupal_set_message(t('File Service List, dropdown selection: @var', array('@var' => $selected)));
            $path = 'suapi/fileservicelist';
            $form_state['redirect'] = array($path);
            return;

          case '10':
            drupal_set_message(t('Place File Order, dropdown selection: @var', array('@var' => $selected)));
            $path = 'suapi/placefileorder';
            $form_state['redirect'] = array($path);
            return;

          case '11':
            $path = 'suapi/placeimeiorder';
            $query = array('serviceid' => $serviceid, 'imei' => $imei);
            $form_state['redirect'] = array($path, array('query' => $query));
            return;

        }
      }
  }
  $form_state['rebuild'] = TRUE;
}

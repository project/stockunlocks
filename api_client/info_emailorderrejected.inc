<?php

/**
 * @file
 * Email template editing: When a submitted order is rejected.
 */

include drupal_get_path('module', 'stockunlocks') . '/includes/stockunlocks.functions.email.inc';

/**
 * Form constructor for the email order rejected form.
 */
function stockunlocks_api_emailorderrejected_form($form, &$form_state) {
  $form = array();

  include '_info_02_top.inc';

  $display_restore_default = TRUE;

  $emailtemplatename = 'Order rejected notification';
  $results = db_query('SELECT * FROM {su_emailtemplates} WHERE Name = :Name', array(':Name' => $emailtemplatename));
  if ($results->rowCount() > 0) {
    foreach ($results as $row) {
      $emailvals['SUBJECT'] = $row->subject;
      $emailvals['MESSAGE'] = $row->message;
      $emailvals['MESSAGEORIG'] = $row->messageorig;
      $emailvals['FROMNAME'] = $row->fromname;
      $emailvals['FROMEMAIL'] = $row->fromemail;
      $emailvals['COPYTO'] = $row->copyto;
      $emailvals['VARIABLES'] = $row->template_variable;
    }
  }

  if (empty($emailvals['MESSAGEORIG']) && $emailvals['MESSAGEORIG'] == NULL) {
    $display_restore_default = FALSE;
  }

  $form['fieldset_second'] = array(
    '#type' => 'fieldset',
    '#weight' => -50,
    '#title' => t('API Email Template Values: <strong>Order Rejected notification</strong>'),
    '#description' => t('<strong>"Update"</strong> to save your changes.<br />
    <strong>"Set Default"</strong> to set new default values.<br />
    <strong>"Restore Default"</strong> to restore default values.<br />
    <br />NOTE: This is the message sent to the customer when the code is unsuccessful.'),
  );

  $form['fieldset_second']['emailtemplatename'] = array(
    '#type' => 'hidden',
    '#value' => $emailtemplatename,
  );

  $default_emailsubject = $emailvals['SUBJECT'];
  $form['fieldset_second']['emailsubject'] = array(
    '#title' => t("Subject:"),
    '#type' => 'textfield',
    '#weight' => -100,
    '#default_value' => $default_emailsubject,
    '#required' => TRUE,
  );

  $default_message = $emailvals['MESSAGE'];
  $form['fieldset_second']['message'] = array(
    '#title' => t('Message:'),
    '#field_prefix' => t('Plain text only.'),
    '#type' => 'textarea',
    '#required' => TRUE,
    '#weight' => -80,
    '#default_value' => $default_message,
  );

  $default_fromname = $emailvals['FROMNAME'];
  $form['fieldset_second']['fromname'] = array(
    '#title' => t("From Name:"),
    '#field_prefix' => t("The name associated with the 'From Email' address."),
    '#type' => 'textfield',
    '#weight' => -70,
    '#default_value' => $default_fromname,
    '#required' => TRUE,
  );

  $default_fromemail = $emailvals['FROMEMAIL'];
  $form['fieldset_second']['fromemail'] = array(
    '#title' => t("From Email:"),
    '#field_prefix' => t('Originates from your website. Usually an admin account.'),
    '#type' => 'textfield',
    '#weight' => -60,
    '#default_value' => $default_fromemail,
    '#required' => TRUE,
  );

  $default_copyto = $emailvals['COPYTO'];
  $form['fieldset_second']['copyto'] = array(
    '#title' => t("Copy to:"),
    '#field_prefix' => t('Send a copy to this address. Usually an admin account.'),
    '#type' => 'textfield',
    '#weight' => -55,
    '#default_value' => $default_copyto,
    '#required' => TRUE,
  );

  $form['fieldset_second']['UPDATE'] = array(
    '#type' => 'submit',
    '#weight' => -50,
    '#value' => t('Update'),
  );

  $form['fieldset_second']['SETDEFAULT'] = array(
    '#type' => 'submit',
    '#weight' => -40,
    '#value' => t('Set Default'),
  );

  if ($display_restore_default) {
    $form['fieldset_second']['RESTORE'] = array(
      '#type' => 'submit',
      '#weight' => -30,
      '#value' => t('Restore Default'),
    );
  }

  $form['emailorderrejected'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email Order Rejected values'),
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $header = array(
    'subject' => t('Subject'),
    'message' => t('Message'),
    'fromname' => t('From Name'),
    'fromemail' => t('From Email'),
    'copyto' => t('Copy to'),
    'variables' => t('Variables'),
  );

  $item = 0;
  $options = array();

  $options[$item] = array(
    'subject' => $emailvals['SUBJECT'],
    'message' => $emailvals['MESSAGE'],
    'fromname' => $emailvals['FROMNAME'],
    'fromemail' => $emailvals['FROMEMAIL'],
    'copyto' => $emailvals['COPYTO'],
    'variables' => $emailvals['VARIABLES'],
  );

  $form['emailorderrejected']['table'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#multiple' => FALSE,
    '#js_select' => FALSE,
    '#disabled' => TRUE,
    '#empty' => t('Email Order Rejected Information is not available. Click to create.'),
  );
  return $form;
}

/**
 * Implements hook_form_validate().
 */
function stockunlocks_api_emailorderrejected_form_validate($form, &$form_state) {
  $flag_continue_fromemail = TRUE;
  $flag_continue_copyto = TRUE;
  $flag_msg_fromemail = '';
  $flag_msg_copyto = '';

  $trigger_value = $form_state['triggering_element']['#value'];

  switch ($trigger_value) {
    case 'Update':
      $trigger_value = 'Submit';
      break;

    case 'Set Default':
      $trigger_value = 'Submit';
      break;

  }

  switch ($trigger_value) {
    case 'Edit':
      if (empty($form_state['values']['api_emailtemplate'])) {
        form_set_error('api_emailtemplate', t('API Email Templates is required.'));
      }
      break;

    case 'Submit':
      $email_fromemail = filter_xss(trim($form_state['values']['fromemail']));
      if (!valid_email_address($email_fromemail)) {
        $flag_continue_fromemail = FALSE;
        $flag_msg_fromemail = t("The 'From' email address ==> @var is not valid.<br />", array('@var' => $email_fromemail));
      }
      $email_copyto = filter_xss(trim($form_state['values']['copyto']));
      if (!valid_email_address($email_copyto)) {
        $flag_continue_copyto = FALSE;
        $flag_msg_copyto = t("The 'Copy to' email address ==> @var is not valid.<br />", array('@var' => $email_copyto));
      }
      break;

  }

  if (!$flag_continue_fromemail) {
    form_set_error('fromemail', $flag_msg_fromemail);
  }
  if (!$flag_continue_copyto) {
    form_set_error('copyto', $flag_msg_copyto);
  }
}

/**
 * Implements hook_form_submit().
 */
function stockunlocks_api_emailorderrejected_form_submit($form, &$form_state) {
  $emailtemplatename = $form_state['values']['emailtemplatename'];
  $emailvals_updated = array();
  $emailvals_updated['SUBJECT'] = filter_xss($form_state['values']['emailsubject']);
  $emailvals_updated['MESSAGE'] = filter_xss($form_state['values']['message']);
  $emailvals_updated['FROMNAME'] = filter_xss($form_state['values']['fromname']);
  $emailvals_updated['FROMEMAIL'] = filter_xss($form_state['values']['fromemail']);
  $emailvals_updated['COPYTO'] = filter_xss($form_state['values']['copyto']);

  switch ($form_state['triggering_element']['#value']) {
    case t('Edit'):
      $selected = $form_state['values']['api_emailtemplate'];
      if ($selected == 0) {
      }
      else {
        $path = 'suapi/emailtemplates';
        switch ($selected) {
          case '1':
            drupal_set_message(t('Placed Order Success - selected.'));
            $path = 'suapi/emailordersuccess';
            $form_state['redirect'] = array($path);
            return;

          case '2':
            drupal_set_message(t('Placed Order Fail: Admin - selected'));
            $path = 'suapi/emailorderfail';
            $form_state['redirect'] = array($path);
            return;

          case '3':
            drupal_set_message(t('Checked Order Fail: Admin - selected'));
            $path = 'suapi/emailcheckorderfail';
            $form_state['redirect'] = array($path);
            return;

          case '4':
            drupal_set_message(t('Order Available - selected'));
            $path = 'suapi/emailorderavailable';
            $form_state['redirect'] = array($path);
            return;

          case '5':
            drupal_set_message(t('Order Rejected - selected'));
            $path = 'suapi/emailorderrejected';
            $form_state['redirect'] = array($path);
            return;

          case '6':
            drupal_set_message(t('E-mail Mass Send - selected'));
            $path = 'suapi/emailmass';
            $form_state['redirect'] = array($path);
            return;

        }
      }
      return;

    case t('Update'):
      stockunlocks_email_template_update($emailvals_updated, $emailtemplatename);
      drupal_set_message(t('Email Template successfully updated.'));
      return;

    case t('Set Default'):
      stockunlocks_email_template_set_default($emailvals_updated, $emailtemplatename);
      drupal_set_message(t('Email Template default values successfully set.'));
      return;

    case t('Restore Default'):
      $emailtemplatename = $form_state['values']['emailtemplatename'];
      stockunlocks_email_template_reset($emailtemplatename);
      drupal_set_message(t('Email Template default values successfully restored.'));
      return;

  }
  $form_state['rebuild'] = TRUE;
}

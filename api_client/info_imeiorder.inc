<?php

/**
 * @file
 * Retrieves an imei order when the order id is supplied.
 */

/**
 * Form constructor for the get imei order form.
 */
function stockunlocks_api_getimeiorder_form($form, &$form_state) {
  $form = array();

  include '_info_01_top.inc';

  $flag_continue = FALSE;

  if (isset($_GET['orderid'])) {
    $api_action_orderid = $_GET['orderid'];
    if (stockunlocks_is_digits($api_action_orderid) != 1) {
      form_set_error('apiid', t('Order ID should be digits only: no letters, punctuation, or spaces!'));
      drupal_set_message(t('Click <a href="/suapi/actions">HERE</a> to try again.'));
      return;

    }
  }

  $para['ID'] = $api_action_orderid;
  $request = $api->action('getimeiorder', $para);
  $i = 0;
  if (is_array($request) && $api_action_orderid != '') {
    $iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($request), RecursiveIteratorIterator::SELF_FIRST);
    foreach ($iterator as $key1 => $val1) {
      if ($key1 === 'ID') {
        $tmp_orderid = $val1;
      }
      if ($key1 === 'SUCCESS' || $key1 === 'ERROR') {
        $flag_continue = TRUE;
        $tmp_result = $key1;
        if (is_array($val1)) {
          foreach ($val1 as $key2 => $val2) {
            if (is_array($val2)) {
              $tmp_msg = '';
              $tmp_imei = '';
              $tmp_status = '';
              $tmp_code = '';
              $tmp_comments = '';
              foreach ($val2 as $key3 => $val3) {
                if ($key3 === 'MESSAGE') {
                  $tmp_msg = $val3;
                }
                if ($key3 === 'IMEI') {
                  $tmp_imei = $val3;
                }
                if ($key3 === 'STATUS') {
                  $tmp_status = $val3;
                }
                if ($key3 === 'CODE') {
                  $tmp_code = $val3;
                }
                if ($key3 === 'COMMENTS') {
                  $tmp_comments = $val3;
                }
              }
              switch ($tmp_result) {
                case 'ERROR':
                  $services = array(
                    'ORDERID' => $tmp_orderid,
                    'RESULT' => $tmp_result,
                    'MESSAGE' => $tmp_msg,
                  );
                  break;

                case 'SUCCESS':
                  $services = array(
                    'ORDERID' => $tmp_orderid,
                    'RESULT' => $tmp_result,
                    'IMEI' => $tmp_imei,
                    'STATUS' => $tmp_status,
                    'CODE' => $tmp_code,
                    'COMMENTS' => $tmp_comments,
                  );
                  break;

              }
            }
          }
        }
      }
    }
  }

  if (!$flag_continue) {

    $form['noexisting'] = array(
      '#type' => 'fieldset',
      '#title' => t('No Existing IMEI Orders'),
      '#weight' => 5,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $header = array(
      'message' => t('Message'),
    );

    $options = array();

    $options[0] = array(
      'message' => t('No existing IMEI Orders found for current API Provider. Click to PLACE an order.'),
    );

    $form['noexisting']['table'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#multiple' => FALSE,
      '#js_select' => FALSE,
      '#disabled' => TRUE,
    );

  }
  else {
    $title = t('IMEI Order for: <strong>@var</strong>', array('@var' => $the_one));
    $form['getimeiorder'] = array(
      '#type' => 'fieldset',
      '#title' => $title,
      '#weight' => 5,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $header = array(
      'order' => t('Order#'),
      'imei' => t('IMEI'),
      'status' => t('Status'),
      'code' => t('Code'),
      'comments' => t('Comments'),
    );

    $item = 0;
    $options = array();

    switch ($services['RESULT']) {
      case 'ERROR':
        $options[$item] = array(
          'order' => $services['ORDERID'],
          'imei' => 'N/A',
          'status' => 'ERROR',
          'code' => 'N/A',
          'comments' => $services['MESSAGE'],
        );
        break;

      case 'SUCCESS':
        $order_status = '';
        switch ($services['STATUS']) {
          case '0':
            $order_status = 'Waiting Action';
            break;

          case '1':
            $order_status = 'In Process';
            break;

          case '3':
            $order_status = 'Rejected';
            break;

          case '4':
            $order_status = 'Success';
            break;

        }
        $options[$item] = array(
          'order' => $services['ORDERID'],
          'imei' => $services['IMEI'],
          'status' => $order_status,
          'code' => $services['CODE'],
          'comments' => $services['COMMENTS'],
        );
        break;

    }

    $form['getimeiorder']['table'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#multiple' => FALSE,
      '#js_select' => FALSE,
      '#disabled' => TRUE,
      '#empty' => t('No existing IMEI Orders found. Click to either:  <a href="/suapi/add">ADD an API Provider</a> OR <a href="/suapi/status">MODIFY an API Provider</a>.'),
    );
  }
  return $form;
}

/**
 * Implements hook_form_validate().
 */
function stockunlocks_api_getimeiorder_form_validate($form, &$form_state) {
  $flag_continue_apiid = TRUE;
  $flag_continue_imei = TRUE;
  $flag_continue_orderid = TRUE;
  $flag_msg_apiid = '';
  $flag_msg_imei = '';
  $flag_msg_orderid = '';

  if ($form_state['triggering_element']['#value'] == 'Execute') {
    $order_id_val = filter_xss($form_state['values']['orderid']);
    $apiid_val = filter_xss($form_state['values']['apiid']);
    $imei_val = filter_xss($form_state['values']['imei']);
    if (!empty($form_state['values']['api_action'])) {
      $api_action_val = $form_state['values']['api_action'];
      // Order ID is Required. User selected "Get IMEI Order".
      if (($api_action_val == '3') && $order_id_val == '') {
        $flag_continue_orderid = FALSE;
        $flag_msg_orderid = t('Order ID is required.<br />');
      }
      else {
        if (($api_action_val == '3') && (stockunlocks_is_digits($order_id_val) != 1)) {
          $flag_continue_orderid = FALSE;
          $flag_msg_orderid = t('Order ID should be digits only: no letters, punctuation, or spaces.<br />');
        }
      }
      // API ID is Required: User selected retrieve.
      // "Model list" or "Provider list".
      if ((($api_action_val == '5') || ($api_action_val == '7')) && $apiid_val != '') {
        if (stockunlocks_is_digits($apiid_val) != 1) {
          $flag_continue_apiid = FALSE;
          $flag_msg_apiid = t('API ID should be digits only: no letters, punctuation, or spaces.<br />');
        }
      }
      // API ID is Required. User requested "IMEI Service Details".
      if (($api_action_val == '6') && $apiid_val == '') {
        $flag_continue_apiid = FALSE;
        $flag_msg_apiid = t('Service API ID is required.<br />');
      }
      else {
        if (($api_action_val == '6') && (stockunlocks_is_digits($apiid_val) != 1)) {
          $flag_continue_apiid = FALSE;
          $flag_msg_apiid = t('API ID should be digits only: no letters, punctuation, or spaces.<br />');
        }
      }
      // IMEI & API ID are Required. User submitted "Place IMEI Order".
      if (($api_action_val == '11') && ($imei_val == '' || $apiid_val == '')) {
        if ($imei_val == '') {
          $flag_continue_imei = FALSE;
          $flag_msg_imei = t('IMEI is required<br />');
        }
        if ($apiid_val == '') {
          $flag_continue_apiid = FALSE;
          $flag_msg_apiid = t('Service API ID is required<br />');
        }
      }
      else {
        if (($api_action_val == '11') && (stockunlocks_is_digits($apiid_val) != 1)) {
          $flag_continue_apiid = FALSE;
          $flag_msg_apiid = t('API ID should be digits only: no letters, punctuation, or spaces.<br />');
        }
        if (($api_action_val == '11') && (stockunlocks_is_digits($imei_val) != 1)) {
          $flag_continue_imei = FALSE;
          $flag_msg_imei = t('IMEI should be digits only: no letters, punctuation, or spaces.<br />');
        }
        if (($api_action_val == '11') && (strlen($imei_val) == 15)) {
          if (stockunlocks_check_imei($imei_val) == 0) {
            $flag_continue_imei = FALSE;
            $flag_msg_imei = t('@var is not a valid IMEI entry.<br />', array('@var' => $imei_val));
          }
        }
      }

      if (!$flag_continue_orderid) {
        form_set_error('orderid', $flag_msg_orderid);
      }
      if (!$flag_continue_apiid) {
        form_set_error('apiid', $flag_msg_apiid);
      }
      if (!$flag_continue_imei) {
        form_set_error('imei', $flag_msg_imei);
      }
    }
    else {
      form_set_error('api_action', t('API Actions is required.'));
    }
  }
}

/**
 * Implements hook_form_submit().
 */
function stockunlocks_api_getimeiorder_form_submit($form, &$form_state) {
  switch ($form_state['triggering_element']['#value']) {
    case t('Execute'):
      $selected = $form_state['values']['api_action'];
      if ($selected == 0) {
      }
      else {
        $path = 'suapi/actions';
        $serviceid = filter_xss(trim($form_state['values']['apiid']));
        $imei = filter_xss(trim($form_state['values']['imei']));
        $orderid = filter_xss(trim($form_state['values']['orderid']));
        switch ($selected) {
          case '1':
            drupal_set_message(t('Account information retrieved.'));
            $path = 'suapi/accountinfo';
            $form_state['redirect'] = array($path);
            return;

          case '2':
            drupal_set_message(t('IMEI Service List retrieved.'));
            $path = 'suapi/imeiservicelist';
            $form_state['redirect'] = array($path);
            return;

          case '3':
            $path = 'suapi/getimeiorder';
            $query = array('orderid' => $orderid);
            $form_state['redirect'] = array($path, array('query' => $query));
            drupal_set_message(t('IMEI Order retrieved: Order#  @var', array('@var' => $orderid)));
            return;

          case '4':
            drupal_set_message(t('MEP List retrieved.'));
            $path = 'suapi/meplist';
            $form_state['redirect'] = array($path);
            return;

          case '5':
            $d_message = '';
            $path = 'suapi/modellist';
            $query = array('serviceid' => $serviceid);
            $form_state['redirect'] = array($path, array('query' => $query));
            if (empty($serviceid) && $serviceid == NULL) {
              $d_message = t('Model List retrieved. No Service ID# supplied.');
            }
            else {
              $d_message = t('Model List retrieved: Service ID# @var', array('@var' => $serviceid));
            }
            drupal_set_message($d_message);
            return;

          case '6':
            $path = 'suapi/getimeiservicedetails';
            $query = array('serviceid' => $serviceid);
            $form_state['redirect'] = array($path, array('query' => $query));
            drupal_set_message(t('IMEI Service Details retrieved: Service ID# @var', array('@var' => $serviceid)));
            return;

          case '7':
            $d_message = '';
            $path = 'suapi/providerlist';
            $query = array('serviceid' => $serviceid);
            $form_state['redirect'] = array($path, array('query' => $query));
            if (empty($serviceid) && $serviceid == NULL) {
              $d_message = t('Provider List retrieved. No Service ID# supplied.');
            }
            else {
              $d_message = t('Provider List retrieved: Service ID# @var', array('@var' => $serviceid));
            }
            drupal_set_message($d_message);
            return;

          case '8':
            drupal_set_message(t('File Order, dropdown selection: @var', array('@var' => $selected)));
            $path = 'suapi/getfileorder';
            $form_state['redirect'] = array($path);
            return;

          case '9':
            drupal_set_message(t('File Service List, dropdown selection: @var', array('@var' => $selected)));
            $path = 'suapi/fileservicelist';
            $form_state['redirect'] = array($path);
            return;

          case '10':
            drupal_set_message(t('Place File Order, dropdown selection: @var', array('@var' => $selected)));
            $path = 'suapi/placefileorder';
            $form_state['redirect'] = array($path);
            return;

          case '11':
            $path = 'suapi/placeimeiorder';
            $query = array('serviceid' => $serviceid, 'imei' => $imei);
            $form_state['redirect'] = array($path, array('query' => $query));
            return;

        }
      }
  }
  $form_state['rebuild'] = TRUE;
}

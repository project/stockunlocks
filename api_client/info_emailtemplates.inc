<?php

/**
 * @file
 * Email template selection: Selecting a specific template for editing.
 */

/**
 * Form constructor for the email templates form.
 */
function stockunlocks_api_emailtemplates_form($form, &$form_state) {
  $form = array();

  include '_info_02_top.inc';
  return $form;
}

/**
 * Implements hook_form_validate().
 */
function stockunlocks_api_emailtemplates_form_validate($form, &$form_state) {

  if ($form_state['triggering_element']['#value'] == 'Edit') {
    if (empty($form_state['values']['api_emailtemplate'])) {
      form_set_error('api_emailtemplate', t('API Email Templates is required.'));
    }
  }
}

/**
 * Implements hook_form_submit().
 */
function stockunlocks_api_emailtemplates_form_submit($form, &$form_state) {
  switch ($form_state['triggering_element']['#value']) {
    case t('Edit'):
      $selected = $form_state['values']['api_emailtemplate'];
      if ($selected == 0) {
      }
      else {
        $path = 'suapi/emailtemplates';
        switch ($selected) {
          case '1':
            drupal_set_message(t('Placed Order Success - selected.'));
            $path = 'suapi/emailordersuccess';
            $form_state['redirect'] = array($path);
            return;

          case '2':
            drupal_set_message(t('Placed Order Fail: Admin - selected'));
            $path = 'suapi/emailorderfail';
            $form_state['redirect'] = array($path);
            return;

          case '3':
            drupal_set_message(t('Checked Order Fail: Admin - selected'));
            $path = 'suapi/emailcheckorderfail';
            $form_state['redirect'] = array($path);
            return;

          case '4':
            drupal_set_message(t('Order Available - selected'));
            $path = 'suapi/emailorderavailable';
            $form_state['redirect'] = array($path);
            return;

          case '5':
            drupal_set_message(t('Order Rejected - selected'));
            $path = 'suapi/emailorderrejected';
            $form_state['redirect'] = array($path);
            return;

          case '6':
            drupal_set_message(t('E-mail Mass Send - selected'));
            $path = 'suapi/emailmass';
            $form_state['redirect'] = array($path);
            return;

        }
      }
      return;

  }
  $form_state['rebuild'] = TRUE;
}

<?php

/**
 * @file
 * Common functions used with the stockunlocks email templates.
 */

/**
 * Implements hook_mail().
 */
function stockunlocks_mail($key, &$message, $params) {
  // Each message is associated with a language, which may or may not be the
  // current user's selected language, depending on the type of e-mail being
  // sent. This $options array is used later in the t() calls for subject
  // and body to ensure the proper translation takes effect.
  $options = array(
    'langcode' => $message['language']->language,
  );

  switch ($key) {
    // Send a simple message.
    case 'contact_message':
      $message['subject'] = t('@subject', array('@subject' => $params['subject']), $options);
      // The message body is an array, not a string.
      // Since user-entered text may have unintentional HTML entities in it like
      // '<' or '>', we need to make sure these entities are properly escaped,
      // as the body will later be transformed from HTML to text, meaning
      // that a normal use of '<' will result in truncation of the message.
      $message['body'][] = check_plain($params['message']);
      break;
  }
}

/**
 * Sends an e-mail.
 *
 * @param array $message_values
 *   An array of values from the contact form fields that were submitted.
 *   There are just two relevant items: $message_values['to'] and
 *   $message_values['message'].
 */
function stockunlocks_mail_send($message_values) {
  // All system mails need to specify the module and template key (mirrored from
  // hook_mail()) that the message they want to send comes from.
  $module = 'stockunlocks';
  // $key = 'contact_message';
  $key = $message_values['key'];

  // Specify 'to' and 'from' addresses.
  $to = $message_values['to'];
  $from = $message_values['from'];

  // "params" loads in additional context for email content completion in
  // hook_mail(). In this case, we want to pass the values
  // which include the message body in $message_values['message'].
  $params = $message_values;
  $language = language_default();

  // Whether or not to automatically send the mail when drupal_mail() is
  // called. This defaults to TRUE, and is normally what you want unless you
  // need to do additional processing before drupal_mail_send() is called.
  $send = TRUE;
  // Send the mail, and check for success. Note that this does not guarantee
  // message delivery; only that there were no PHP-related issues encountered
  // while sending.
  $result = drupal_mail($module, $key, $to, $language, $params, $from, $send);

  return $result['result'];
}

/**
 * Updates the current values for the selected email template.
 *
 * @param array $emailvals_updated
 *   An associative array containing:
 *   - subject: A string representing the email template subject line.
 *   - message: A string representing the email template message.
 *   - fromname: A string representing the email template "from" name.
 *   - fromemail: A string representing the email template "from" email address.
 *   - copyto: A string representing the email template "copy to" email address.
 *
 * @param string $emailtemplatename
 *   A string representing the name of an email template.
 */
function stockunlocks_email_template_update($emailvals_updated, $emailtemplatename) {
  $num_updated = db_update('su_emailtemplates')
  ->fields(array(
      'subject' => $emailvals_updated['SUBJECT'],
      'message' => $emailvals_updated['MESSAGE'],
      'fromname' => $emailvals_updated['FROMNAME'],
      'fromemail' => $emailvals_updated['FROMEMAIL'],
      'copyto' => $emailvals_updated['COPYTO'],
      ))
  ->condition('Name', $emailtemplatename)
  ->execute();
}

/**
 * Updates the default values for the selected email template.
 *
 * @param array $emailvals_updated
 *   An associative array containing:
 *   - subject: A string representing the email template subject line.
 *   - message: A string representing the email template message.
 *   - fromname: A string representing the email template "from" name.
 *   - fromemail: A string representing the email template "from" email address.
 *   - copyto: A string representing the email template "copy to" email address.
 *
 * @param string $emailtemplatename
 *   A string representing the name of an email template.
 */
function stockunlocks_email_template_set_default($emailvals_updated, $emailtemplatename) {
  $num_updated = db_update('su_emailtemplates')
  ->fields(array(
      'messageorig' => serialize($emailvals_updated),
      ))
  ->condition('Name', $emailtemplatename)
  ->execute();
}

/**
 * Resets the selected email template to its default values.
 *
 * @param string $emailtemplatename
 *   A string representing the name of an email template.
 */
function stockunlocks_email_template_reset($emailtemplatename) {
  $message_values = array();

  $messageorig = db_query('SELECT messageorig FROM {su_emailtemplates} WHERE Name = :Name', array(':Name' => $emailtemplatename))->fetchField();

  $message_values = unserialize($messageorig);
  $num_updated = db_update('su_emailtemplates')
  ->fields(array(
      'subject' => $message_values['SUBJECT'],
      'message' => $message_values['MESSAGE'],
      'fromname' => $message_values['FROMNAME'],
      'fromemail' => $message_values['FROMEMAIL'],
      'copyto' => $message_values['COPYTO'],
      ))
  ->condition('Name', $emailtemplatename)
  ->execute();
}

/**
 * Sends a specific mass notification to customers.
 *
 * @param string $emailtemplatename
 *   A string representing the name of an email template.
 */
function stockunlocks_email_template_mass($emailtemplatename, $emailstatus, $send_test) {
  // Collect all matching status orders.
  $order_status = $emailstatus;
  $result_orders_ordered = array();
  $result_orders_ordered = db_query("SELECT * FROM {uc_orders} WHERE order_status = :order_status", array(':order_status' => $order_status));
  $num_orders = $result_orders_ordered->rowCount();
  if (!empty($result_orders_ordered) && $result_orders_ordered <> NULL) {
    $i = 0;
    $msgs_sent = 0;
    $order_values = array();
    $apiprovider_name = '';
    foreach ($result_orders_ordered as $record_orders_ordered) {
      $order_values['order_id'] = $record_orders_ordered->order_id;
      $order_values['uid'] = $record_orders_ordered->uid;
      $order_values['order_status'] = $record_orders_ordered->order_status;
      $order_values['order_total'] = $record_orders_ordered->order_total;
      $order_values['qty'] = $record_orders_ordered->product_count;
      $order_values['primary_email'] = $record_orders_ordered->primary_email;
      ++$i;
      $result_admin_comments_arr = array();
      $result_admin_comments_arr[] = $result_admin_comments = db_query("SELECT * FROM {uc_order_admin_comments} WHERE order_id = :order_id", array(':order_id' => $order_values['order_id']));
      foreach ($result_admin_comments_arr as $result) {
        foreach ($result as $record) {
          $order_values['order_id'] = $record->order_id;
          $results = explode('-php-', $record->message);
          foreach ($results as $entry => $values) {
            // Extract the Ubercart order information.
            switch ($order_status) {
              case 'su_ordered_pending':
                $order_status = 'su_ordered';
                break;
            }
            if ($values == $order_status) {
              $order_values['api_provider'] = $results[1];
              $order_values['api_order_id'] = $results[2];
              $order_values['IMEI'] = $results[3];
              $order_values['RespondEmail'] = $results[4];
              $order_values['delivery_first_name'] = $results[5];
              $order_values['title'] = $results[7];
              $order_values['PhoneInfo'] = $results[8];
              $order_values['order_time'] = $results[9];
              $order_values['timestrict'] = $results[10];
              $order_values['timestrictvalue'] = $results[11];
              $order_values['payment_method'] = $results[12];
              $order_values['order_time_diff'] = time() - $order_values['order_time'];
              $order_values['order_time_hours'] = $order_values['order_time_diff'] / 3600;
              $flag_timechk = TRUE;
              $apiprovider_name = db_query('SELECT sitename FROM {su_api_provider} WHERE ID = :ID', array(':ID' => $order_values['api_provider']))->fetchField();
              $order_values['apiprovider_name'] = $apiprovider_name;

              // Build the email notification.
              $order_mass_subject = '';
              $order_mass_message = '';
              $order_mass_fromname = '';
              $order_mass_fromemail = '';
              $order_mass_copyto = '';
              $results_order_mass_admin = db_query('SELECT * FROM {su_emailtemplates} WHERE Name = :Name', array(':Name' => $emailtemplatename));
              if ($results_order_mass_admin->rowCount() > 0) {
                foreach ($results_order_mass_admin as $row) {
                  $order_mass_subject = $row->subject;
                  $order_mass_message = $row->message;
                  $order_mass_fromname = $row->fromname;
                  $order_mass_fromemail = $row->fromemail;
                  $order_mass_copyto = $row->copyto;
                }
                $replace = array(
                  '{$imei}' => $order_values['IMEI'],
                  '{$orderid}' => $order_values['order_id'],
                );
                $order_mass_subject = stockunlocks_string_replace_assoc($replace, $order_mass_subject);
                $replace = array(
                  '{$customerfirstname}' => $order_values['delivery_first_name'],
                  '{$imei}' => $order_values['IMEI'],
                  '{$orderid}' => $order_values['order_id'],
                  '{$phoneinfo}' => $order_values['PhoneInfo'],
                  '{$service}' => $order_values['title'],
                );
                $order_mass_message = stockunlocks_string_replace_assoc($replace, $order_mass_message);
              }
              $order_values['order_time'] = time();

              $emailsubject = $order_mass_subject;
              $comments_order = $order_mass_message;

              // Notify the customer.
              $body = $comments_order;
              $notified = 0;
              $message_values = array();
              $message_values['key'] = 'contact_message';
              $message_values['from'] = $order_mass_fromname . " <" . $order_mass_fromemail . ">";
              $message_values['to'] = $order_values['RespondEmail'];
              $message_values['subject'] = $emailsubject;
              $message_values['message'] = $body;

              ++$msgs_sent;

              if ($send_test == 1) {
                $message_values['to'] = $order_mass_copyto;
                $message_values['subject'] = 'TEST - FIRST MESSAGE: ' . $emailsubject;
                $message_values['message'] = 'THIS IS A TEST MESSAGE THAT WOULD HAVE BEEN SENT TO: ' . $num_orders . ' ORDER(s)' . chr(10) . chr(10) . $body;
                stockunlocks_mail_send($message_values);
                drupal_set_message(t('Test message sent.'));
                return;
              }
              else {
                // If successful, send the admin a copy with additional info.
                if (stockunlocks_mail_send($message_values)) {
                  $body = 'Customer email: ' . $order_values['RespondEmail'] . "\r\n" . $comments_order . "\r\n\r\n" . 'API Provider: ' . $order_values['apiprovider_name'];
                  $emailsubject = 'cc: ' . $emailsubject;
                  $message_values['to'] = $order_mass_copyto;
                  $message_values['subject'] = $emailsubject;
                  $message_values['message'] = $body;
                  stockunlocks_mail_send($message_values);
                  $notified = 1;
                }
                else {
                  $notified = 0;
                }
                // Insert the order comments into the table.
                if ($notified == 1) {
                  $id = db_insert('uc_order_comments')
                  ->fields(array(
                    'order_id' => $order_values['order_id'],
                    'uid' => 1,
                    'order_status' => $order_status,
                    'notified' => $notified,
                    'message' => $comments_order,
                    'created' => time(),
                  ))
                  ->execute();
                }
                else {
                  // Notify admin that message not sent.
                }
              }
            }
          }
        }
      }
    }
  }
  drupal_set_message(t('Total messages sent = @var', array('@var' => $msgs_sent)));
}

<?php

/**
 * @file
 * Functionality that is common across multiple files.
 */

// These constants are essential for all front end API pages.
define("REQUESTFORMAT", "JSON");
define("SU_AID_BRAND", variable_get('stockunlocks_aid_su_brand_id'));
define("SU_AID_COUNTRY", variable_get('stockunlocks_aid_su_country_id'));
define("SU_AID_EMAIL_CONFIRM", variable_get('stockunlocks_aid_su_email_confirm'));
define("SU_AID_EMAIL_RESPONSE", variable_get('stockunlocks_aid_su_email_response'));
define("SU_AID_IMEI", variable_get('stockunlocks_aid_su_imei'));
define("SU_AID_MEP", variable_get('stockunlocks_aid_su_mep_id'));
define("SU_AID_MODEL", variable_get('stockunlocks_aid_su_model_id'));
define("SU_AID_NETWORK", variable_get('stockunlocks_aid_su_network_id'));
define("SU_AID_NOTES", variable_get('stockunlocks_aid_su_notes'));
define("SU_AID_PHONE_NUM", variable_get('stockunlocks_aid_su_activation_number'));
define("SU_AID_KBH", variable_get('stockunlocks_aid_su_kbh'));

/**
 * Implements hook_init().
 */
function stockunlocks_init() {
  // Use these to prevent copy/paste when viewing a stocunlocks product.
  drupal_add_js(array(
    'stockunlocks' => array(
      'su_aid_email_response' => SU_AID_EMAIL_RESPONSE,
      'su_aid_email_confirm' => SU_AID_EMAIL_CONFIRM,
    ),
  ), 'setting');
}

// Essential, dynamically created files for order verification,
// processing and importing of services: Dhru API interaction.
// See: api_dynamic/api_full_dhru_template.txt for the example.
$dirpath = variable_get('file_private_path', conf_path() . '/files/private');
$dirpath = $dirpath . '/stockunlocks_api_dynamic/api_full_dhru.inc';

if (file_exists($dirpath)) {
  include $dirpath;
}

/**
 * Returns a string, typically a simple page formatted as HTML.
 */
function stockunlocks_test_page_generate() {
  return "<p>This is a test php page.</p>";
}

/**
 * Returns a string for a str_replace output.
 *
 * @param array $replace
 *   An associative array containing key/value pairs.
 *
 * @param string $subject
 *   A string representing the target of the replacement values.
 */
function stockunlocks_string_replace_assoc(array $replace, $subject) {
  return str_replace(array_keys($replace), array_values($replace), $subject);
}

/**
 * Returns a multidimensional array, disabling of the submit button.
 *
 * @param array $element
 *   A multidimensional array to be provided for a hook_form_afterbuild()
 */
function stockunlocks_load_submit_disable_javascript($element) {
  drupal_add_js(drupal_get_path('module', 'stockunlocks') . '/js/stockunlocks_api_submit_disable.js');
  return ($element);
}

/**
 * Returns an int, flagging for digits only, no decimals or characters.
 *
 * @param string $element
 *   A string representation of a numerical value.
 */
function stockunlocks_is_digits($element) {
  return !preg_match("/[^0-9]/", $element);
}

/**
 * Returns an int, confirming the validity of an IMEI.
 *
 * @param string $imei
 *   A string representation of a 15 digit IMEI number.
 */
function stockunlocks_check_imei($imei) {
  $dig = 0;
  for ($i = 0; $i < 14; $i += 2) {
    $cdigit = $imei[$i + 1] << 1;
    $dig += $imei[$i] + (int) ($cdigit / 10) + ($cdigit % 10);
  }
  $dig = (10 - ($dig % 10)) % 10;
  if ($dig == $imei[14]) {
    return 1;
  }
  else {
    return 0;
  }
}

/**
 * Returns an associative array, extracting key/value pairs.
 *
 * @param array $array
 *   A multidimensional array containing key/value pairs at various levels.
 *
 * @param array $vals
 *   An associative array to contain the extracted values.
 */
function stockunlocks_array_keys_multi_values(array $array, &$vals) {
  $keys = array();
  foreach ($array as $key => $value) {
    $keys[] = $key;
    if (is_array($array[$key])) {
      $keys = array_merge($keys, stockunlocks_array_keys_multi_values($array[$key], $vals));
    }
    if (is_array($value)) {
      stockunlocks_array_keys_multi_values($value, $vals);
    }
    else {
      $vals[$key] = $value;
    }
  }
  return $keys;
}

<?php

/**
 * @file
 * Common functions used with the stockunlocks shopping cart.
 */

/**
 * Returns an indexed array that contains the brand name.
 *
 * @param string $assigned_brand_txt
 *   A comma concatenated string value representing a list of assigned brands.
 *
 * @param string $api_provider
 *   A string value representing the unique identifier of a service provider.
 */
function stockunlocks_cart_fetch_brand($assigned_brand_txt, $api_provider) {
  $options[''] = t('---');
  $assigned_brand = array();
  $assigned_brand = explode(',', $assigned_brand_txt);
  asort($assigned_brand, SORT_NUMERIC);
  foreach ($assigned_brand as $api_brand) {
    $result_brand = db_query("SELECT * FROM {su_service_brand} WHERE api = :api AND api_provider = :api_provider",
                            array(
                              ':api' => $api_brand,
                              ':api_provider' => $api_provider,
                            ));
    foreach ($result_brand as $record_int) {
      $tmp_bid = $record_int->api;
      $tmp_bname = $record_int->Name;
      $options[$tmp_bid] = $tmp_bname;
    }
  }
  asort($options);
  return $options;
}

/**
 * Returns an indexed array that ontains the model name.
 *
 * @param string $assigned_model_txt
 *   A comma concatenated string value representing a list of assigned models.
 *
 * @param string $api_brand
 *   A string value representing the unique identifier of a brand.
 *
 * @param string $api_provider
 *   A string value representing the unique identifier of a service provider.
 */
function stockunlocks_cart_fetch_model($assigned_model_txt, $api_brand, $api_provider) {
  $options[''] = t('---');
  $assigned_model = array();
  $assigned_model = explode(',', $assigned_model_txt);
  asort($assigned_model, SORT_NUMERIC);
  $result_model_int = db_query("SELECT * FROM {su_service_model} WHERE ID_Brand = :ID_Brand AND api_provider = :api_provider",
                              array(
                                ':ID_Brand' => $api_brand,
                                ':api_provider' => $api_provider,
                              ));
  foreach ($result_model_int as $record_int) {
    // Only include the models allowed by this service.
    if (in_array($record_int->api, $assigned_model)) {
      $tmp_mid = $record_int->api;
      $tmp_mname = $record_int->Name;
      $options[$tmp_mid] = $tmp_mname;
    }
  }
  asort($options);
  return $options;
}

/**
 * Returns an indexed array that contains the MEP name.
 *
 * @param string $api_provider
 *   A string value representing the unique identifier of a service provider.
 */
function stockunlocks_cart_fetch_mep($api_provider) {
  $options[''] = t('---');
  $result = db_query("SELECT * FROM {su_mep} WHERE api_provider = :api_provider",
                    array(
                      ':api_provider' => $api_provider,
                    ));
  foreach ($result as $record) {
    $tmp_id = $record->api;
    $tmp_name = $record->Name;
    $options[$tmp_id] = $tmp_name;
  }
  $options = array_unique($options);
  asort($options);
  return $options;
}

/**
 * Returns an indexed array that contains the country name.
 *
 * @param string $assigned_provider_txt
 *   A comma concatenated string of a list of network providers.
 *
 * @param string $api_provider
 *   A string value representing the unique identifier of a service provider.
 */
function stockunlocks_cart_fetch_country($assigned_provider_txt, $api_provider) {
  $options[''] = t('---');
  $assigned_provider = array();
  $flag_provider = FALSE;
  if (!empty($assigned_provider_txt) && $assigned_provider_txt != NULL) {
    $flag_provider = TRUE;
    $assigned_provider = explode(',', $assigned_provider_txt);
    asort($assigned_provider, SORT_NUMERIC);
  }
  else {
    $flag_provider = FALSE;
  }
  // Return specific countries based on allowed network providers.
  if ($flag_provider) {
    foreach ($assigned_provider as $record_provider) {
      $result_network = db_query("SELECT ID_Country,api FROM {su_service_network} WHERE api = :api AND api_provider = :api_provider GROUP BY ID_Country",
                                array(
                                  ':api' => $record_provider,
                                  ':api_provider' => $api_provider,
                                ));
      foreach ($result_network as $record_network) {
        $result_country = db_query("SELECT * FROM {su_service_country} WHERE api = :api",
                                  array(
                                    ':api' => $record_network->ID_Country,
                                  ));
        foreach ($result_country as $record_country) {
          $tmp_id = $record_country->api;
          $tmp_name = $record_country->Name;
          $options[$tmp_id] = $tmp_name;
        }
      }
    }
  }
  else {
    // Return all countries.
    $result_network = db_query("SELECT ID_Country FROM {su_service_network} WHERE api_provider = :api_provider GROUP BY ID_Country",
                              array(':api_provider' => $api_provider,
                              ));
    foreach ($result_network as $record_network) {
      $result_country = db_query("SELECT * FROM {su_service_country} WHERE api = :api",
                                array(
                                  ':api' => $record_network->ID_Country,
                                ));
      foreach ($result_country as $record_country) {
        $tmp_id = $record_country->api;
        $tmp_name = $record_country->Name;
        $options[$tmp_id] = $tmp_name;
      }
    }
  }
  asort($options);
  return $options;
}

/**
 * Returns an indexed array that contains the network provider name.
 *
 * @param string $assigned_provider_txt
 *   A comma concatenated string value of a list of network providers.
 *
 * @param string $selected_country
 *   A string value representing the unique identifier of a country.
 *
 * @param string $api_provider
 *   A string value representing the unique identifier of a service provider.
 */
function stockunlocks_cart_fetch_network($assigned_provider_txt, $selected_country, $api_provider) {
  $options[''] = t('---');
  $assigned_provider = array();
  $flag_provider = FALSE;
  if (!empty($assigned_provider_txt) && $assigned_provider_txt != NULL) {
    $flag_provider = TRUE;
    $assigned_provider = explode(',', $assigned_provider_txt);
    asort($assigned_provider, SORT_NUMERIC);
  }
  else {
    $flag_provider = FALSE;
  }
  // Return specific network provider(s) based on allowed network providers.
  if ($flag_provider) {
    foreach ($assigned_provider as $record_provider) {
      $result_network = db_query("SELECT * FROM {su_service_network} WHERE ID_Country = :ID_Country AND api = :api AND api_provider = :api_provider",
                                array(
                                  ':ID_Country' => $selected_country,
                                  ':api' => $record_provider,
                                  ':api_provider' => $api_provider,
                                ));
      foreach ($result_network as $record_network) {
        $tmp_id = $record_network->api;
        $tmp_name = $record_network->Name;
        $options[$tmp_id] = $tmp_name;
      }
    }
  }
  else {
    // Return all network providers.
    $result_network = db_query("SELECT * FROM {su_service_network} WHERE ID_Country = :ID_Country AND api_provider = :api_provider",
                              array(
                                ':ID_Country' => $selected_country,
                                ':api_provider' => $api_provider,
                              ));
    foreach ($result_network as $record_network) {
      $tmp_id = $record_network->api;
      $tmp_name = $record_network->Name;
      $options[$tmp_id] = $tmp_name;
    }
  }
  asort($options);
  return $options;
}

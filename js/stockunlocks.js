/**
 * @file
 * Utility functions to display when js turned off
 * Utility functions to copy/past and submit button disablement.
 */

(function($) {

  Drupal.behaviors.stockunlocks = {
    attach: function() {
      // Chck if ajax is enabled in order to
      // re-enable form elements that are disabled for non-ajax situations.
      if (Drupal.ajax) {
        $('.enabled-for-ajax').removeAttr('disabled');
      }

      // Below is only for showing with js turned off.
      // It overrides the behavior of the CSS that would normally turn off
      // the 'ok' button when JS is enabled. Here we have AJAX disabled
      // but JS turned on, this is used to simulate.
      if (!Drupal.ajax) {
        $('html.js .stockunlocks-cart-next-button').show();
      }

      // Prevent copy/pasting into email fields.
      $('input').bind('copy paste', function(e) {
        var val = this.name;
        var su_aid_email_1 = "attributes[" + Drupal.settings.stockunlocks.su_aid_email_response + "]";
        var su_aid_email_2 = "attributes[" + Drupal.settings.stockunlocks.su_aid_email_confirm + "]";
        if (val == su_aid_email_1 || val == su_aid_email_2) {
          e.preventDefault();
        }
      });

      // Only Add to Cart one time when clicked multiple times
      $('form').submit(function() {
        jQuery(".form-submit").click(function() {
          jQuery(this).attr("disabled","disabled");
        });
      });

    }
  };

})(jQuery);

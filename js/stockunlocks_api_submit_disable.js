/**
 * @file
 * Utility functions to disble submit button on the actions pages.
 */

(function($) {

  Drupal.behaviors.stockunlocks = {
    attach: function() {
      // Prevent double submissions when placing an IMEI order.
      $('#edit-api-action').change(function() {
        var val = $(this).val();
        if (val == 11) {
          jQuery(".form-submit").click(function() {
            jQuery(this).attr("disabled","disabled");
          });
        }
      });
    }
  };

})(jQuery);

<?php

/**
 * @file
 * Dynamically generated connection details for a designated Service Provider.
 * This file is used for 'Actions' found under the menu: /suapi/actions
 * This is from Dhru Fusion, the API remote client enabled connection.
 * @link http://wiki.dhru.com/Client_Remote_APi Dhru Client Remote API. @endlink
 */

define('DHRUFUSION_URL', "http://reseller.website.com/");
define("USERNAME", "username");
define("API_ACCESS_KEY", "XXX-XXX-XXX-XXX-XXX-XXX-XXX-XXX");

if (!extension_loaded('curl')) {
  trigger_error('cURL extension not installed', E_USER_ERROR);
}

/**
 * StockUnlocks Dhru Fusion api.
 */
class StockunlocksDhruFusion {
  public $xmldata;
  public $xmlresult;
  public $debug;
  public $action;

  /**
   * Initializes xmldata.
   */
  public function __construct() {
    $this->xmldata = new DOMDocument();
  }

  /**
   * Return the xml results.
   */
  public function getResult() {
    return $this->xmlresult;
  }

  /**
   * Connect with the remote client.
   */
  public function action($action, $arr = array()) {
    if (is_string($action)) {
      if (is_array($arr)) {
        if (count($arr)) {
          $request = $this->xmldata->createElement("PARAMETERS");
          $this->xmldata->appendChild($request);
          foreach ($arr as $key => $val) {
            $key = strtoupper($key);
            $request->appendChild($this->xmldata->createElement($key, $val));
          }
        }
        $posted = array(
          'username' => USERNAME,
          'apiaccesskey' => API_ACCESS_KEY,
          'action' => $action,
          'requestformat' => REQUESTFORMAT,
          'parameters' => $this->xmldata->saveHTML(),
        );
        $crul = curl_init();
        // curl_setopt($crul, CURLOPT_INTERFACE, '00.000.00.00'); By default.
        curl_setopt($crul, CURLOPT_HEADER, FALSE);
        curl_setopt($crul, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        // curl_setopt($crul, CURLOPT_FOLLOWLOCATION, TRUE); By default.
        curl_setopt($crul, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($crul, CURLOPT_URL, DHRUFUSION_URL . '/api/index.php');
        curl_setopt($crul, CURLOPT_POST, TRUE);
        curl_setopt($crul, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($crul, CURLOPT_POSTFIELDS, $posted);
        $response = curl_exec($crul);
        if (curl_errno($crul) != CURLE_OK) {
          echo curl_error($crul);
          curl_close($crul);
        }
        else {
          curl_close($crul);
          if ($this->debug) {
            // Echo "<textarea rows='20' cols='200'> "; Echo = echo, By default.
            // print_r($response); By default.
            // echo "</textarea>"; By default.
          }
          return (json_decode($response, TRUE));
        }
      }
    }
    return FALSE;
  }
}
